# This is a general purpose nginx front end for lad-mirror

Note: There is a report generated from the following splunk query that depends on these containers logs being default nginx logs. See below for reference:

```splunk
index=oit_okd source="http:okd-3-prod" kubernetes.namespace_name="lad-mirror" "kubernetes.labels.app"="lad-archive-frontend" "kubernetes.container_name"=nginx code=2* NOT agent=*bot* NOT agent=*spider* NOT agent=*slurp* NOT agent=kube-probe* | rex "GET /(?:pub/)?(?<REPO>\w+)" | search REPO!=pub | eval gb=size/1024/1024/1024 | convert timeformat="%m-%d-%Y" ctime(_time) AS date  | stats sum(gb) by date, REPO | outputlookup lad_bandwidth_cache.csv
```

To install use `kubectl apply -k kustomize/` from this directory with the target namespace in your current context.

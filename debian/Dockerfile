FROM debian:bullseye
LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Office of Information Technology, OIT SSI Systems'
LABEL architecture='x86_64'
LABEL summary='Linux@Duke Mirroring Service debian-ftpsync image'
LABEL description='Base image for operating Debian portion of the Linux@Duke mirror'
LABEL distribution-scope='private'
LABEL authoritative-source-url='https://gitlab.oit.duke.edu/linux-at-duke/lad-mirror'

LABEL version='1.0'
LABEL release='1'

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y \
      ca-certificates \
      git \
      rsync \
      httpie && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /archvsync && \
    git clone https://salsa.debian.org/mirror-team/archvsync.git /archvsync && \
    chgrp -R 0 /archvsync && \
    chmod -R g=u /archvsync

RUN mkdir /.httpie && chgrp -R 0 /.httpie && chmod -R g+rwX /.httpie    

ENV PATH /archvsync/bin:$PATH

ADD ftpsync.conf /archvsync/etc/ftpsync.conf
ADD debian.mirror /debian.mirror

RUN mkdir /debian && \
    chgrp -R 0 /debian && \
    chmod -R g=u /debian

ENTRYPOINT ["/debian.mirror"]

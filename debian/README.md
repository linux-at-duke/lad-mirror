# Notes on debian mirror?

there are two secrets in the openshift project

1. gitlab-trigger to allow git lab to trigger the build in
    - this one only exists in the project, it's url gets stuck in "Integrations>Webhooks" in the gitlab ui
2. gitlab-pull to allow openshift to pull from linux-at-duke/lad-mirror since it's an "internal" project

The cronjob in here can be run ad hoc like this:
  `oc create job --from=cronjob/run-debian-ftpsync debian-ftpsync-oneshot`


Openshift Egress

The Linux@Duke mirror has special access to NFS storage dedicated to the service. To enable access controls applied to this storage the lad-mirror openshift project has a dedicated egress IP which is visible in:
```
# .cloud.duke.edu
$ oc get netnamespace lad-mirror
```
This IP is enabled ad the default egress for this project

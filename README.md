This repo will serve as a jumping off point for building the images needed to operate the Linux@Duke mirroring service.


migration process (WIP!!)

- get a sync working on the 'lad-mirror-nfs-volume' pvc (this is testish space on oit-nas-fe11: https://packrat.oit.duke.edu/volumes/918 )
  - keep in mind this is roughly 1.5T whereas our entire mirror set is just over 12T
- when you're ready to take the new sync production:
  - go into the production nas space lad-archive-02:/srv/mirror/${project}
  - make the following chgrp/chmod changes to allow the pods to update the production nas space:
    - chgrp -R 0 /srv/mirror/${project} # note, this takes a **while**
    - chmod g+rwX -R /srv/mirror/${project}
  - stop the cronjob on lad-archive-02 relevant to your project
    - you can remove or comment out the line in puppet_old/private_files/lad-archive-02.linux.duke.edu/mirror.cron # be sure to run puppet on lad-archive-02 after svn updating
  - make the lad-mirror project user the owner of the files in your project
    - chown -v 1000190000 -R /srv/mirror/${project}

# Testing Sync

Your OKD yaml file should contain a cronjob that runs periodically.  If you
would like to test a run immediately, you can use the following command:

```
oc create job --from cronjob/run-${YOUR_CRONJOB_NAME} ${YOUR_CRONJOB_NAME}-manual-$(date +%s)
```

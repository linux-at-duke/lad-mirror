```crontab
51 */12  * * * fedora /opt/duke/bin/slockrun.py time /srv/quick-fedora-mirror/quick-fedora-mirror -T 'last day' &> /var/log/fedora.mirror
53 */6  * * * fedora /srv/bin/epel.mirror &>/dev/null
1  *    * * * mirror /srv/bin/ius.mirror --randomize &>/dev/null
12 */4  * * * ubuntu /usr/bin/lockrun --lockfile=/tmp/ubuntu.lock -- /srv/bin/ubuntu.mirror &>/dev/null
18 9  * * * mirror /opt/duke/bin/slockrun.py /srv/bin/centos.mirror &>/dev/null
00 22  * * * mirror /opt/duke/bin/slockrun.py /srv/bin/ovirt.mirror &>/dev/null
23 5    * * * mirror /srv/bin/sl.mirror &>/dev/null
44 2    * * * mirror /srv/bin/cran.mirror &>/dev/null
30 3    * * * mirror /srv/bin/bioconductor.mirror &>/dev/null
40 4    * * * mirror /srv/bin/pypi.mirror &>/dev/null
00 9,21 * * * root /srv/bin/mirror_sizes.py >/dev/null
23 */6 * * * debian /srv/debian-mirror/bin/ftpsync sync:archive:debian
23 */12 * * * debian /srv/debian-mirror/bin/ftpsync sync:archive:debian-cd
15 4 * * * kalilinux /home/kalilinux/bin/mirror-kali-images
```

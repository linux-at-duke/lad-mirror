#!/bin/bash

set -e

if [[ "${1}" -eq "" ]]; then
   rsync --port 8873 --daemon --no-detach --log-file /dev/stdout
else
    exec ${@}
fi
